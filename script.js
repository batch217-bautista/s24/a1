// console.log("Hello World!");

// 1.
const getCube = Math.pow(2, 3);
console.log(`The cube of 2 is ${getCube}`);


// 2.
const address = {
    houseNumber : 258,
    street: "Washington Ave.",
    state: "NW",
    zipcode: 90011
};


// 3.
const {houseNumber, street, state, zipcode} = address;
console.log(`I live at ${houseNumber} ${street} ${state} ${zipcode}`);

const biggestCroc = {
    name: "Lolong",
    species: "Saltwater Crocodile",
    weight: 1075,
    measurement: "20 ft 3 in"
};

const {name, species, weight,measurement} = biggestCroc;
console.log(`${name} was a ${species}. He weighed at ${weight} kgs with a measurement of ${measurement}.`);

// 4.

const printNum = [1, 2, 3, 4, 5, 15];

printNum.forEach((reduceNumber) => {
    console.log(`${reduceNumber}`);
});

// 5.
class Dog {
    constructor(name, age, breed) {
        this.name = name,
        this.age = age,
        this.breed = breed
    }
}

const myDog = new Dog();
myDog.name = "Samael";
myDog.age = 2;
myDog.breed = "Wolfdog";

console.log(myDog);